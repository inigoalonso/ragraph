from pathlib import Path

from ragraph import plot
from ragraph.graph import Graph
from ragraph.io.grip import from_grip


def test_grip_format(datadir: Path, tmpdir: Path):
    g: Graph = from_grip(datadir / "grip" / "grip.xml")
    assert g.nodes, "I want nodes."

    fig = plot.mdm(
        g.leafs, g.edges, style=plot.Style(piemap=(dict(display="kinds", mode="relative")))
    )
    fig.write_image(tmpdir / "grip.svg", format="svg")
